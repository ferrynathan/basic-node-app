FROM node:lts-alpine3.15
ENV NODE_ENV production
WORKDIR /app
COPY --chown=node:node package.json yarn.lock ./
RUN yarn install --production --immutable && yarn cache clean
COPY --chown=node:node . .
EXPOSE 3000
CMD ["node", "src/index.js"]
